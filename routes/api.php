<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\OrderProductController;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth',AuthController::class);
Route::apiResource('products',ProductController::class)->middleware('isAdmin');
Route::middleware('isAdmin')
    ->controller(OrderController::class)
    ->prefix('orders')
    ->name('orders.')
    ->group(function(){
        Route::get('/','index')->name('index');
        Route::post('/','store')->name('store');
        Route::get('/{order}','show')->name('show');
        Route::put('/{order}','update')->name('update');
        Route::delete('/{order}','destroy')->name('destroy');
    });    
