<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Order;
use App\Models\Product;

class OrderProductsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' =>  Order::inRandomOrder()->first()->id,
            'product_id' =>  Product::inRandomOrder()->first()->id,
            'price' => $this->faker->numberBetween(29,105),
            'quantity' => $this->faker->numberBetween(1,100)
        ];
    }
}
