<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderProducts;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //create table without foreign key constraints.
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Truncate method remove all rows from a table.
        DB::table('users')->truncate();
        DB::table('orders')->truncate();
        DB::table('products')->truncate();
        DB::table('order_products')->truncate();
        User::factory(10)->create();
        Product::factory(100)->create();

        Order::factory(20)->create()->each(function($order){
           $nb = rand(2,5);
            OrderProducts::factory($nb)->create([
                'order_id'   =>  $order->id,
            ]);
         });
    }
}
