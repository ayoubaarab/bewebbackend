<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\OrderProducts;
use App\Models\Order;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        $orders->load('products');
        return OrderResource::collection($orders);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {   
        $data = $request->validated(); 
        $order = Order::fill($data);
        $order->save();
        foreach( $data['products'] as $product ):
            $productObj = (object) $product;
            OrderProducts::create([
                'order_id'   => $order->id,
                'product_id' => $productObj->product_id,
                'quantity'   => $productObj->quantity,
                'price'      => $productObj->price
            ]);
        endforeach;    
        return new OrderResource($order);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order->products = $order->products;
        return new OrderResource($order);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, Order $order)
    {
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */

    public function destroy(Order $order)
    {
        $order->products()->delete();
        $order->delete();
        return new OrderResource($order);
    }
}
