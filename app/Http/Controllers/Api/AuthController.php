<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;

class AuthController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(AuthRequest $request)
    {
        $data = $request->validated();
        $dbEmail = "test@gmail.com";
        $dbPassword = "test";
        return ($data['email'] !== $dbEmail || $data['password'] !== $dbPassword )
                ? response()->json(['message' => 'Email or Password wrong!!'],401)
                : response()->json(['token'=>'123456789'],200);
    }            
}
