<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderProductsResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function amount($orderProducts){
        $amount = 0;
        foreach($orderProducts as $orderProduct){
            $orderProductObj = (object) $orderProduct;
            $amount += $orderProductObj->price * $orderProductObj->quantity;
        }
        return $amount;
    }
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'firtsname'  => $this->firstname,
            'lastname'   => $this->lastname,
            'products'   => $this->products,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'amount'     =>  $this->amount(OrderProductsResource::collection($this->products)),
        ];
    }
}
