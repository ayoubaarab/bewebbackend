<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:255|min:3',
            'lastname'  => 'required|max:255|min:3',
            'products.*.product_id' => 'required',
            'products.*.price' => 'required',
            'products.*.quantity' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'firstname.required' => 'A firtsname is required',
            'lastname.required'  => 'A lastname is required',
            'products.*.product_id.required' => 'your order broduct must containe a product_id',
            'products.*.price.required' => 'order product must have price field',
            'products.*.quantity.required' => 'order product must have quantity field',
                ];
    }
}
