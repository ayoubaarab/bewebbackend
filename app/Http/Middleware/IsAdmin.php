<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $staticToken = "123456789";
        $bearerToken = $request->bearerToken();
        if( empty($bearerToken) || $bearerToken !== $staticToken ):
            return response()->json(['message' => 'Ops Access Denied !!'],401);
        endif;
        return $next($request);
    }
}
