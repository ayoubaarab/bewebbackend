<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['firstname','lastname'];

    public function products()
    {
        return $this->hasMany(OrderProducts::class);
    }
    
    public function getFirstnameAttribute($value)
    {
        return ucfirst($value);
    }

}
