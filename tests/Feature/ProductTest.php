<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Product;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->token = "123456789" ;
    }

    public function test_index()
    {

        $response = $this->withHeaders([
                'Authorization' => 'Bearer '. $this->token,
            ])->get('/api/products');
       
        $response->assertJsonCount(Product::whereNull('deleted_at')->count());

        $response->assertStatus(200);
    }

    public function test_store()
    {
        $product =  Product::factory()->make()->toArray();
        $response = $this->withHeaders([
                            'Authorization' => 'Bearer '. $this->token,
                        ])->postJson('/api/products' , $product);

        $response->assertStatus(201);
    }

    public function test_update()
    {
        $updatedProduct = Product::factory()->make()->toArray();
        $product = Product::latest()->first();
        $response = $this->withHeaders([
                            'Authorization' => 'Bearer '. $this->token
                        ])->putJson('/api/products/'. $product->id, $updatedProduct);

        $response->assertStatus(200); 
    }
     public function test_delete()
     {
        $product = Product::latest()->first();
        $response = $this->withHeaders([
                            'Authorization' => 'Bearer '. $this->token
                        ])->deleteJson('/api/products/'. $product->id);

        $response->assertStatus(200);
     }

}
