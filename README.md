
# beweb backend api

to strat poject run: 
```
php artisan serve
php artisan migrate:fresh --seed

```
For Readme Syntax please read : https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax

## firts copy .env.example to a new file call .env
## using git bash in your root directory run: cp .env.example .env
	it will create new file call .env with the same data that we have in .env.example.
## create new database in your machine (mamp Or xampp...)
## make nesseseary changes in .env file at lines 11 12 13 14 15 16.
## in root project directory run: php artisan migrate --seed
	to create the migration on your database and insert the generated fake data.
## to start the app run: php artisan serve
## Mirations and data rolations.
	On this app we have three migration orders,producs,order_producs
	(Many to Many relation).
	Each product has many ordres and each order has many products.
	To define this relationship, we need to create a third table (in addition to the two tables on both sides of the relationship) in our database (order_products).

## we generate only one middleware "isAdmin" you can make any change desi
	red you can find IsAdmin class in this path "app/Http/Middleware/IsAdmin.php"
## validation section
		at this level all validation's rule are set on "app/Http/Request"
 	so by injecting the ClassNameRequest on any method on your class, it will work fine. and in case the validation is faild the errors will be returned dynamicaly to you api. all need to to do is set Headers to (Accept: application/json) in case is missing on your http you will be get a response comes from web index route.
## routes
	on web route section: we use regular expression to prevent request api routes from the web section. 
	"you can visite regex101.com site for more info about regex"
	All Api requests handled on api.php file.
## Testing
	to make tests please open your project root directory and run: php artisan test
	All tests will be found under tests/Feature.


